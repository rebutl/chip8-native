#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "chip8.h"

// Each char is 4 pixels wide (hence the size of the hex value)
// and 5 pixels tall (hence 5 hex values per row)
unsigned char 	fontset[80] =
{
	0xF0, 0x90, 0x90, 0x90, 0xF0,	// 0
	0x20, 0x60, 0x20, 0x20, 0x70, 	// 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, 	// 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, 	// 3
	0x90, 0x90, 0xF0, 0x10, 0x10, 	// 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, 	// 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, 	// 6
	0xF0, 0x10, 0x20, 0x40, 0x40, 	// 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, 	// 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, 	// 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, 	// A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, 	// B
	0xF0, 0x80, 0x80, 0x80, 0xF0, 	// C
	0xE0, 0x90, 0x90, 0x90, 0xE0, 	// D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, 	// E
	0xF0, 0x80, 0xF0, 0x80, 0x80  	// F
};

Chip8::Chip8()
{

}

Chip8::~Chip8()
{

}

void Chip8::initialize()
{
	// Initialise registers and memory once
	pc		= 0x200;		// Program starts at 0x200
	opcode	= 0;			// Reset current opcode
	I		= 0;			// Reset index register
	sp		= 0;			// Reset stack pointer

	// Clear display
	for(int i = 0; i < HEIGHT*WIDTH; ++i)
		gfx[i] = 0;

	// Clear stack
	for(int i = 0; i < STACK_DEPTH; ++i)
		stack[i] = 0;

	// Clear registers V0-VF
	for(int i = 0; i < REGISTER_COUNT; ++i)
		V[i] = 0;

	// Clear the keys
	for(int i = 0; i < KEY_COUNT; i++)
		key[i] = 0;

	// Clear memory
	for(int i = 0; i < MEMORY_SIZE; ++i)
		memory[i] = 0;

	// Load the fontset
	for(int i = 0; i < 80; ++i)
		memory[i] = fontset[i];

	// reset timers
	delay_timer = 0;
	sound_timer = 0;

	// Clear screen once
	drawFlag = true;

	srand (time(NULL));
}

bool Chip8::loadGame(const char * strGame)
{
	initialize();
	printf("Loading %s...\n", strGame);
	FILE *pFile;
	pFile = fopen(strGame, "rb");

	if(!pFile)
	{
		printf("Rom not found\n\n");
		return false;
	}

	// Check file size
	fseek(pFile, 0, SEEK_END);
	long lSize = ftell(pFile);
	rewind(pFile);
	printf("Filesize: %d\n", (int)lSize);
	// Allocate memory to contain the whole file
	char *buffer = (char*)malloc(sizeof(char) * lSize);

	if(!buffer)
	{
		printf("Error allocating rom data\n\n");
		return false;
	}

	// Copy the file into the buffer
	size_t result = fread(buffer, 1, lSize, pFile);

	if(result != lSize)
	{
		printf("Error reading rom data\n\n");
		return false;
	}

	// Copy buffer to Chip8 memory
	if((MEMORY_SIZE - 512) > lSize)
	{
		for(int i = 0; i < lSize; ++i)
			memory[i + 512] = buffer[i];
	}
	else
		printf("Error: ROM too large for memory");

	// Close file and free buffer
	fclose(pFile);
	free(buffer);

	return true;
}

void Chip8::emulateCycle()
{
	// Fetch opcode
	opcode = memory[pc] << 8 | memory[pc + 1];

	// Decode and execute opcode
	// Each time we execute an opcode we increase pc by 2 (each opcode is 2
	// bytes long). This only changes if you jump or call a subroutine
	// We can filter out most opcodes by the first 4 bits
	// Process opcode
	switch(opcode & 0xF000)
	{
	case 0x0000:
		switch(opcode & 0x000F)
		{
		case 0x0000: // 0x00E0: Clears the screen
			for(int i = 0; i < HEIGHT*WIDTH; ++i)
				gfx[i] = 0x0;
			drawFlag = true;
			pc += 2;
			break;
		case 0x000E: // 0x00EE: Returns from subroutine
			--sp;			// 16 levels of stack, decrease stack pointer to prevent overwrite
			pc = stack[sp];	// Put the stored return address from the stack back into the program counter
			pc += 2;		// Don't forget to increase the program counter!
			break;

		default:
			printf ("Unknown opcode [0x0000]: 0x%X\n", opcode);
			break;
		}
		break;
	case 0x1000: // 0x1NNN: Jumps to address NNN
		pc = (opcode & 0x0FFF);
		break;
	case 0x2000: // 0x2NNN: Calls subroutine at NNN.
		stack[sp] = pc;			// Store current address in stack
		++sp;					// Increment stack pointer
		pc = opcode & 0x0FFF;	// Set the program counter to the address at NNN
		break;
	case 0x3000: // 0x3XNN: Skips the next instruction if VX equals NN
		if(V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
			pc += 4;
		else
			pc += 2;
		break;
	case 0x4000: // 0x4XNN: Skips the next instruction if VX doesn't equal NN
		if(V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
			pc += 4;
		else
			pc += 2;
		break;
	case 0x5000: // 0x5XY0: Skips the next instruction if VX equals VY.
		if(V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
			pc += 4;
		else
			pc += 2;
		break;
	case 0x6000: // 0x6XNN: Sets VX to NN.
		V[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
		pc += 2;
		break;
	case 0x7000: // 0x7XNN: Adds NN to VX.
		V[(opcode & 0x0F00) >> 8] += opcode & 0x00FF;
		pc += 2;
		break;
	case 0x8000:
		switch(opcode & 0x000F)
		{
		case 0x0000: // 0x8XY0: Sets VX to the value of VY
			V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;
		case 0x0001: // 0x8XY1: Sets VX to "VX OR VY"
			V[(opcode & 0x0F00) >> 8] |= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;
		case 0x0002: // 0x8XY2: Sets VX to "VX AND VY"
			V[(opcode & 0x0F00) >> 8] &= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;
		case 0x0003: // 0x8XY3: Sets VX to "VX XOR VY"
			V[(opcode & 0x0F00) >> 8] ^= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;
		case 0x0004: // 0x8XY4: Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't
			if(V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8]))
				V[0xF] = 1; //carry
			else
				V[0xF] = 0;
			V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;
		case 0x0005: // 0x8XY5: VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't
			if(V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
				V[0xF] = 0; // there is a borrow
			else
				V[0xF] = 1;
			V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;
		case 0x0006: // 0x8XY6: Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift
			V[0xF] = V[(opcode & 0x0F00) >> 8] & 0x1;
			V[(opcode & 0x0F00) >> 8] >>= 1;
			pc += 2;
			break;
		case 0x0007: // 0x8XY7: Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't
			if(V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])	// VY-VX
				V[0xF] = 0; // there is a borrow
			else
				V[0xF] = 1;
			V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;
		case 0x000E: // Shifts VX left by 1, VF = the MSB of VX before shift
			V[0xF] = V[(opcode & 0x0F00) >> 8] >> 7;
			V[(opcode & 0x0F00) >> 8] <<= 1;
			pc += 2;
			break;
		default:
			printf("Unknown opcode 0x%X\n", opcode);
			break;
		}
		break;
	case 0x9000:	// Skips the next instruction if VX doesn't equal VY
		if(V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
		    pc += 4;
		else
			pc += 2;
		break;
	case 0XA000:	// Sets I to the address NNN
		I = opcode & 0x0FFF;
		pc += 2;
		break;
	case 0xB000:	// Jumps to the address NNN plus V0
		pc = (opcode & 0x0FFF) + V[0];
		break;
	case 0xC000:	// Sets VX to a random number and NN
		V[(opcode & 0x0F00) >> 8] = (rand() % 0xFF) & (opcode & 0x00FF);
		pc += 2;
		break;
	case 0xD000:// Draws a sprite at VX,VY, width = 8, height = N
	{
		// each row of 8 pixels is read as bit-coded starting from memory
		// location I;
		// I value doesn't change after the execution of this instruction
		// VF is set to 1 if any screen pixels are flipped from set to unset
		// when the sprite is drawn, and to 0 if that doens't happen
		unsigned short x = V[(opcode & 0x0F00) >> 8];
		unsigned short y = V[(opcode & 0x00F0) >> 4];
		unsigned short height = opcode & 0x000F;
		unsigned short pixel;

		V[0xF] = 0;

		for(int yline = 0; yline < height; yline++)
		{
			pixel = memory[I + yline];
			for(int xline = 0; xline < 8; xline++)
			{
				// Compare each pixel in this row this is on
				if((pixel & (0x80 >> xline)) != 0)
				{
					// check to see if this pixel is on (will be turned off)
					if(gfx[(x + xline + ((y + yline) * HEIGHT))] == 1)
					{
						V[0xF] = 1;
					}
					// update the value of the pixel
					gfx[x + xline + ((y + yline) * HEIGHT)] ^= 1;
				}
			}
		}
		drawFlag = true;
		pc += 2;
	}
	break;
	case 0xE000:
		switch(opcode & 0x00FF)
		{
		case 0x009E:// Skips the next instruction if the key in VX is pressed
			if(key[V[(opcode & 0x0F00) >> 8]] != 0)
				pc += 4;
			else
				pc += 2;
			break;
		case 0x00A1:// Skips the next instruction if the key in VS isn't pressed
			if(key[V[(opcode & 0x0F00) >> 8]] == 0)
				pc += 4;
			else
				pc += 2;
			break;
		default:
			printf("Unknown opcode 0x%X\n", opcode);
			break;
		}
		break;
	case 0XF000:
		switch(opcode & 0x00FF)
		{
		case 0x0007:// Sets VX to the value of the delay timer
			V[(opcode & 0x0F00) >> 8] = delay_timer;
			pc += 2;
			break;
		case 0x000A:// A key press is awaited, and then stored in VX
		{
			bool keyPress = false;

			for(int i = 0; i < KEY_COUNT; ++i)
			{
				if(key[i] != 0)
				{
					V[(opcode & 0x0F00) >> 8] = i;
					keyPress = true;
				}
			}

			// didn't receive a keypress, skip this cycle and try again
			if(!keyPress)
				return;

			pc += 2;
		}
			break;
		case 0x0015:// Sets the delay timer to VX
			delay_timer = V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;
		case 0x0018:// Sets the sound timer to VX
			sound_timer = V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;
		case 0x001E:// Adds VX to I
			I += V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;
		case 0x0029:// Sets I to the location of the sprite for the character VX
			// I = the spot in the fontset where the character at VX starts
			I = V[(opcode & 0x0F00) >> 8] * 0x5;
			pc += 2;
			break;
		case 0x0033:// Stores binary representation of VX
			// Most significant of three digits at the address of I
			// The middle digit at I + 1, and LSD at I + 2
			memory[I]		= V[(opcode & 0x0F00) >> 8] / 100;
			memory[I + 1]	= (V[(opcode & 0x0F00) >> 8] / 10) % 10;
			memory[I + 2]	= (V[(opcode & 0x0F00) >> 8] % 100) % 10;
			pc += 2;
			break;
		case 0x0055:// Stores V0 to VX in memory starting at address I
			for(int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
				memory[I + i] = V[i];
			// On the original interpreter, when the operation is done, I=I+X+1
			I += ((opcode & 0x0F00) >> 8) + 1;
			pc += 2;
			break;
		case 0x0065:// Fills V0 to VX with values starting at address I
			for(int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
				 V[i] = memory[I + i];

			// On the original interpreter, when the operation is done, I=I+X+1
			I += ((opcode & 0x0F00) >> 8) + 1;

			pc += 2;
			break;
		default:
			printf("Unknown opcode 0x%X\n", opcode);
			break;
		}
		break;
	default:
		printf("Unknown opcode 0x%x\n", opcode);
		break;
	}

	// Update timers
	if(delay_timer > 0)
		--delay_timer;
	if(sound_timer > 0)
	{
		if(sound_timer == 1)
			printf("Beep!\n");
		--sound_timer;
	}
}

void Chip8::dumpState()
{
	printf("Stack:\n");
	for(int i = 0; i < STACK_DEPTH; ++i)
		printf("%d: %d\n", i, stack[i]);
	printf("Registers:\n");
	for(int i = 0; i < REGISTER_COUNT; ++i)
		printf("V%d: %d\n", i, V[i]);
}
