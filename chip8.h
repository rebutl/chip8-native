/*
 * chip8.h
 *
 *  Created on: 2012-04-12
 *      Author: andrew
 */
#ifndef CHIP8_H_
#define CHIP8_H_

#define MEMORY_SIZE		4096
#define REGISTER_COUNT	16
#define HEIGHT			64
#define WIDTH			32
#define STACK_DEPTH		16
#define KEY_COUNT		16

class Chip8
{
private:
	unsigned short	opcode;
	unsigned char	memory[MEMORY_SIZE];
	unsigned char 	V[REGISTER_COUNT];
	unsigned short	I;					// Index register
	unsigned short 	pc;					// Program Counter
	unsigned char	delay_timer;
	unsigned char	sound_timer;
	unsigned short 	stack[STACK_DEPTH];
	unsigned short 	sp;

	void initialize();
	/*
	 * Systems memory map:
	 * 0x000 - 0x1FF - Chip8 interpreter (contains font set in emu)
	 * 0x050 - 0x0A0 - Used for the built in 4x5 pixel font set (0-F)
	 * 0x200 - 0xFFF - Program ROM and work RAM
	 */
public:
	unsigned char 	gfx[HEIGHT * WIDTH];
	unsigned char 	key[KEY_COUNT];
	bool 			drawFlag;

	Chip8();
	~Chip8();
	bool loadGame(const char* strGame);
	void emulateCycle();
	void dumpState();
};

#endif /* CHIP8_H_ */
